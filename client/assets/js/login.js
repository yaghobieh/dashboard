(function() {
    const registerForm = document.getElementsByClassName('register-form');
    const loginForm = document.getElementsByClassName('login-form');

    //If already exist
    if(localStorage.user){
        const loginUser = JSON.parse(localStorage.user);
        if(loginUser.token) {
            registerForm[0].style.display = 'none';
            loginForm[0].style.display = 'none';
            document.getElementsByClassName('already-logged')[0].style.display = 'block';
        }
    }

    //Add event listener to button (Login & logout)
    document.getElementById('submit-login').addEventListener("click", submitLogin);
    document.getElementById('logout').addEventListener("click", logOut);
    document.getElementById('create').addEventListener("click", create);
    document.getElementById('signin').addEventListener("click", signIn);
    document.getElementById('register').addEventListener("click", register);
})();

function register(e) {
    e.preventDefault();
    const firstName = document.getElementById('firstName');
    const lastName = document.getElementById('lastName');
    const userName = document.getElementById('usernameReg');
    const password = document.getElementById('passwordReg');
    createNewUserService(firstName.value, lastName.value, userName.value, password.value);
}

function createNewUserService(firstName, lastName, userName, password) {
    var raw = JSON.stringify({
        "firstName": firstName ,
        "lastName": lastName,
        "username": userName,
        "password": password
    });

    const requestOptions = {
        method: 'POST',
        headers: {
            "Content-Type": "application/json"
        },
        body: raw,
        redirect: 'follow'
    };

    fetch("http://localhost:4000/users/register", requestOptions)
        .then(response => response.text())
        .then(result => {
            window.location.reload(true);
        })
        .catch(error => console.log('error', error));
}

function signIn(e) {
    e.preventDefault();
    const register = document.getElementsByClassName('register-form');
    const login  = document.getElementsByClassName('login-form');
    register[0].style.display = 'none';
    login[0].style.display = 'block';
}

function create(e) {
    e.preventDefault();
    const register = document.getElementsByClassName('register-form');
    const login  = document.getElementsByClassName('login-form');
    register[0].style.display = 'block';
    login[0].style.display = 'none';
}

// Submit login function
function submitLogin(e) {
    e.preventDefault();
    const username = document.getElementsByName('username');
    const password = document.getElementsByName('password');
    makeLogin(username[0].value, password[0].value);
}

// Login request
function makeLogin(username, password) {
    const loginStatusMsg = document.getElementsByClassName('login-status-ok');
    const user = JSON.stringify({
        "username":username,
        "password":password
    });

    let requestOptions = {
        method: 'POST',
        headers: {
            "Content-Type": "application/json"
        },
        body: user,
        redirect: 'follow'
    };

    fetch("http://localhost:4000/users/authenticate", requestOptions)
        .then(response => response.text())
        .then(result => {
            const res = JSON.parse(result)
            localStorage.setItem('user', result);
            loginStatusMsg[0].style.display = 'block';
            setCookie('user', result, 1);
            window.location.reload(true);
        })
        .catch(error => console.log('error', error));
}

// Logout
function logOut() {
    localStorage.removeItem('user');
    const loginForm = document.getElementsByClassName('login-form');
    loginForm[0].style.display = 'block';
    document.getElementsByClassName('already-logged')[0].style.display = 'none';
    eraseCookie('user');
}

// Cookie Manager
function setCookie(name,value,days) {
    let expires = "";
    if (days) {
        const date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}

function getCookie(name) {
    let nameEQ = name + "=";
    let ca = document.cookie.split(';');
    for(let i=0;i < ca.length;i++) {
        let c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function eraseCookie(name) {
    document.cookie = name+'=; Max-Age=-99999999;';
}