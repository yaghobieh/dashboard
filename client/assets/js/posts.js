(function() {
   if(localStorage.user) {
       const user = JSON.parse(localStorage.user);
       getAllPostsUser(user);
   } else {
       localStorage.removeItem('user');
       window.location.replace('/index');
   }
   document.getElementById('newPost').addEventListener('click', newPostMaker);
   document.getElementById('esc').addEventListener('click', esc);
   document.getElementById('savePost').addEventListener('click', savePost);
   document.getElementById('logout').addEventListener('click', logout);
})();

function logout() {
    localStorage.removeItem('user');
    window.location.replace('/index.html');
}

function savePost(e) {
    e.preventDefault();
    const title = document.getElementById('titlePost');
    const content = document.getElementById('contentPost');
    saveNewPost(title.value, content.value);
}

function esc(e) {
    e.preventDefault();
    const newPostForm = document.getElementsByClassName('flow-form');
    newPostForm[0].style.display = 'none';
}

function newPostMaker(e) {
    e.preventDefault();
    const newPostForm = document.getElementsByClassName('flow-form');
    newPostForm[0].style.display = 'block';
}

function saveNewPost(title, content) {
    if(localStorage.user) {
        const user = JSON.parse(localStorage.user);
        const token = 'Bearer ' + user.token
        console.log(token);
        const post = JSON.stringify({
            "title": title,
            "content": content,
            "userId": user.id
        });

        const requestOptions = {
            method: 'POST',
            headers: {
                'Authorization' : token,
                'Content-Type' : 'application/json'
            },
            body: post,
            redirect: 'follow'
        };

        fetch("http://localhost:4000/posts/create_new_post", requestOptions)
            .then(response => response.text())
            .then(result =>{
                console.log(result);
                window.location.reload(true);
            })
            .catch(error => console.log('error', error));
    }
}

function getAllPostsUser(user) {
    const token = 'Bearer ' + user.token
    const requestOptions = {
        method: 'GET',
        headers: {
            'Authorization' : token,
            'Content-Type' : 'application/json'
        },
        redirect: 'follow'
    };

    const parent = document.getElementById('posts-parent');

    let url = 'http://localhost:4000/posts/get_user_post/' + user.id;
    fetch(url, requestOptions)
        .then(response => response.json())
        .then(result => {
            result.map(post => {
                const li = document.createElement('li');
                const content = document.createElement('div');
                const date = document.createElement('div');
                const h2 = document.createElement('h2');
                const p = document.createElement('p');
                content.className = 'content';
                date.className = 'date';
                const title = document.createTextNode(post.title);
                const contentText = document.createTextNode(post.content);
                const nDate = document.createTextNode(post.date);
                p.appendChild(contentText);
                h2.appendChild(title);
                date.appendChild(nDate);
                content.appendChild(h2);
                content.appendChild(p);
                li.appendChild(content);
                li.appendChild(date);
                parent.appendChild(li);
            })
        })
        .catch(error => console.log('error', error));
}
