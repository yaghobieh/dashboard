const express = require('express');
const router = express.Router();
const postService = require('./posts.services');
const mongoose = require('mongoose');

// routes
router.get('/get_user_post/:id', getPosts);
router.post('/create_new_post', createNewPost);

module.exports = router;

function getPosts(req, res, next) {
    const id = mongoose.Types.ObjectId(req.params.id);
    postService.getAllPosts(id)
        .then(posts => posts ? res.json(posts) : res.sendStatus(404))
        .catch(err => next(err));
}

function createNewPost(req, res, next) {
    const postObject = {
        title: req.body.title,
        content: req.body.content,
        userId: mongoose.Types.ObjectId(req.body.userId)
    };
    postService.createNewPost(postObject)
        .then(posts => posts ? res.json(posts) : res.sendStatus(404))
        .catch(err => next(err));
}