const config = require('config.json');
const db = require('_helpers/db');
const Post = db.Post;

module.exports = {
    getAllPosts,
    createNewPost
}

async function getAllPosts(userId) {
    let respose = Post.find({'userId': userId}, (err, result) => {
        if(err || !result)  throw 'No post found';
        return result;
    });
    return respose;
}

async function createNewPost(postObject) {
    if(!postObject.title || !postObject.content || !postObject. userId) throw 'Some field is missed';
    const post = new Post(postObject);
    await post.save();
}