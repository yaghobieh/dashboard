const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    title: { type: String, unique: true },
    date: { type: Date, default: new Date() },
    content: { type: String },
    userId: { type: Schema.Types.ObjectId }
});

module.exports = mongoose.model('Posts', schema);