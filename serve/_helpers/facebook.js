const queryString = require('query-string');

const stringifiedParams = queryString.stringify({
    client_id: '962009234286711',
    redirect_uri: 'https://www.example.com/authenticate/facebook/',
    scope: ['email', 'user_friends'].join(','), // comma seperated string
    response_type: 'code',
    auth_type: 'rerequest',
    display: 'popup',
});