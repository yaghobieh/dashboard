﻿require('rootpath')();
const express = require('express');
const app = express();
const cors = require('cors');
const logger = require('morgan');
const bodyParser = require('body-parser');
const jwt = require('_helpers/jwt');
const errorHandler = require('_helpers/error-handler');

app.use(logger('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
require('dotenv').config();
app.use(cors());
app.use(jwt()); // use JWT auth to secure the api

// api routes
app.use('/users', require('./users/users.controller'));
app.use('/posts', require('./posts/posts.controller'));

// global error handler
app.use(errorHandler);

const port = process.env.NODE_ENV == 'dev' ? process.env.PORT_DEV : process.env.PORT_PROD;;
app.listen(port, function () {
    console.log('Server listening on port ' + port);
});
